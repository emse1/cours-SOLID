# 1 - Slides du cours SOLID

Le contenu du cours magistral se trouve dans le fichier `./presentation/presentation.html`, slides animées en JS.

Pour y accéder via un petit serveur HTTP : 

- Lancer `./run.sh` à la racine du dépot
- Éditer le contenu des slides dans le fichier `slides.md`
- Accéder à la présentation via l'URL : `http://localhost:8000/presentation/presentation.html`


